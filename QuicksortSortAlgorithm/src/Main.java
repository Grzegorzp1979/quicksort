import java.util.Random;

public class Main {

	public static void main(String[] args) {

		Random myRandom = new Random();
		int[] array = new int[10];

		for (int i = 0; i < array.length; i++) {
			array[i] = myRandom.nextInt(100);
		}

		System.out.println("Before: ");
		print(array);
		quitSort(array, 0, array.length - 1);
		System.out.println();
		System.out.println("After: ");
		print(array);
	}
	// print method
	private static void print(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
	}
	// quitsort method
	public static void quitSort(int[] array, int lowIndex, int highIndex) {
		
		if (lowIndex >= highIndex) {
			return;
		}
		
		int pivot = array[highIndex];
		int leftPointer = lowIndex;
		int rightPointer = highIndex;
		
		while (leftPointer < rightPointer) {
			while (array[leftPointer] <= pivot && leftPointer < rightPointer) {
				leftPointer++;
			}
			
			while (array[rightPointer] >= pivot && leftPointer < rightPointer) {
				rightPointer--;
			}
			
			swap(array, leftPointer, rightPointer);
		}
		
		swap(array, leftPointer, highIndex);
		
		quitSort(array, lowIndex, leftPointer - 1);
		quitSort(array, leftPointer + 1, highIndex);
	}
	// swap method
	private static void swap(int[] array, int index1, int index2) {
		int temp = array[index1];
		array[index1] = array[index2];
		array[index2] = temp;
	}
}
